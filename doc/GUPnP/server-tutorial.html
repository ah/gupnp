<!--
SPDX-FileCopyrightText: The GUPnP developers

SPDX-License-Identifier: LGPL-2.1-or-later
-->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>GUPnP &ndash; 1.6: Implementing UPnP devices</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta charset="utf-8" />

  
  <meta property="og:type" content="website"/>

  
  <meta property="og:image:width" content="256"/>
  <meta property="og:image:height" content="256"/>
  <meta property="og:image:secure_url" content="gupnp-logo-short.svg"/>
  <meta property="og:image:alt" content="GUPnP-1.6"/>
  

  
  <meta property="og:title" content="GUPnP: Implementing UPnP devices"/>
  <meta property="og:description" content="Reference for GUPnP-1.6: Implementing UPnP devices"/>
  <meta name="twitter:title" content="GUPnP: Implementing UPnP devices"/>
  <meta name="twitter:description" content="Reference for GUPnP-1.6: Implementing UPnP devices"/>


  
  <meta name="twitter:card" content="summary"/>

  
  
  
  

  <link rel="stylesheet" href="style.css" type="text/css" />

  

  
  <script src="urlmap.js"></script>
  
  
  <script src="fzy.js"></script>
  <script src="search.js"></script>
  
  <script src="main.js"></script>
  <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
  <div id="body-wrapper" tabindex="-1">

    <nav class="sidebar devhelp-hidden">
      
      <div class="section">
        <img src="gupnp-logo-short.svg" class="logo"/>
      </div>
      
      
      <div class="search section">
        <form id="search-form" autocomplete="off">
          <input id="search-input" type="text" name="do-not-autocomplete" placeholder="Click, or press 's' to search" autocomplete="off"/>
        </form>
      </div>
      
      <div class="section namespace">
        <h3><a href="index.html">GUPnP</a></h3>
        <p>API Version: 1.6</p>
        
        <p>Library Version: 1.6.0</p>
        
      </div>
      
      
    </nav>

    <button id="btn-to-top" class="hidden"><span class="up-arrow"></span></button>

    
<section id="main" class="content">
  <section>
    <div class="docblock">
    <h1 id="upnp-server-tutorial">UPnP Server Tutorial<a class="md-anchor" href="#upnp-server-tutorial" title="Permanent link"></a></h1>
<p>This chapter explains how to implement an UPnP service using GUPnP. For
this example we will create a virtual UPnP-enabled light&nbsp;bulb.</p>
<p>Before any code can be written, the device and services that it implement
need to be described in <span class="caps">XML</span>.  Although this can be frustrating, if you are
implementing a standardised service then the service description is
already written for you and the device description is trivial.  UPnP has
standardised Lighting Controls, so we&#8217;ll be using the device and service types defined&nbsp;there.</p>
<h2 id="defining-the-device">Defining the Device<a class="md-anchor" href="#defining-the-device" title="Permanent link"></a></h2>
<p>The first step is to write the <em>device description</em>
file.  This is a short <span class="caps">XML</span> document which describes the device and what
services it provides (for more details see the <a href="http://upnp.org/specs/arch/UPnP-arch-DeviceArchitecture-v1.0.pdf">UPnP Device Architecture specification</a>, section 2.1).  We&#8217;ll be using
the <code>BinaryLight1</code> device type, but if none of the
existing device types are suitable a custom device type can be&nbsp;created.</p>
<div class="codehilite"><pre><span></span><code><span class="cp">&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</span>
<span class="nt">&lt;root</span> <span class="na">xmlns=</span><span class="s">&quot;urn:schemas-upnp-org:device-1-0&quot;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;specVersion&gt;</span>
    <span class="nt">&lt;major&gt;</span>1<span class="nt">&lt;/major&gt;</span>
    <span class="nt">&lt;minor&gt;</span>0<span class="nt">&lt;/minor&gt;</span>
  <span class="nt">&lt;/specVersion&gt;</span>

  <span class="nt">&lt;device&gt;</span>
    <span class="nt">&lt;deviceType&gt;</span>urn:schemas-upnp-org:device:BinaryLight:1<span class="nt">&lt;/deviceType&gt;</span>
    <span class="nt">&lt;friendlyName&gt;</span>Kitchen Lights<span class="nt">&lt;/friendlyName&gt;</span>
    <span class="nt">&lt;manufacturer&gt;</span>OpenedHand<span class="nt">&lt;/manufacturer&gt;</span>
    <span class="nt">&lt;modelName&gt;</span>Virtual Light<span class="nt">&lt;/modelName&gt;</span>
    <span class="nt">&lt;UDN&gt;</span>uuid:cc93d8e6-6b8b-4f60-87ca-228c36b5b0e8<span class="nt">&lt;/UDN&gt;</span>

    <span class="nt">&lt;serviceList&gt;</span>
      <span class="nt">&lt;service&gt;</span>
        <span class="nt">&lt;serviceType&gt;</span>urn:schemas-upnp-org:service:SwitchPower:1<span class="nt">&lt;/serviceType&gt;</span>
        <span class="nt">&lt;serviceId&gt;</span>urn:upnp-org:serviceId:SwitchPower:1<span class="nt">&lt;/serviceId&gt;</span>
        <span class="nt">&lt;SCPDURL&gt;</span>/SwitchPower1.xml<span class="nt">&lt;/SCPDURL&gt;</span>
        <span class="nt">&lt;controlURL&gt;</span>/SwitchPower/Control<span class="nt">&lt;/controlURL&gt;</span>
        <span class="nt">&lt;eventSubURL&gt;</span>/SwitchPower/Event<span class="nt">&lt;/eventSubURL&gt;</span>
      <span class="nt">&lt;/service&gt;</span>
    <span class="nt">&lt;/serviceList&gt;</span>
  <span class="nt">&lt;/device&gt;</span>
<span class="nt">&lt;/root&gt;</span>
</code></pre></div>

<p>The <code>&lt;specVersion&gt;</code> tag defines what version of the UPnP
Device Architecture the document conforms to.  At the time of writing the
only version is&nbsp;1.0.</p>
<p>Next there is the root <code>&lt;device&gt;</code> tag.  This contains
metadata about the device, lists the services it provides and any
sub-devices present (there are none in this example).  The
<code>&lt;deviceType&gt;</code> tag specifies the type of the&nbsp;device.</p>
<p>Next we have <code>&lt;friendlyName&gt;</code>, <code>&lt;manufacturer&gt;</code> and <code>&lt;modelName&gt;</code>. The
friendly name is a human-readable name for the device, the manufacturer
and model name are&nbsp;self-explanatory.</p>
<p>Next there is the <span class="caps">UDN</span>, or <em>Unique Device Name</em>. This
is an identifier which is unique for each device but persistent for each
particular device.  Although it has to start with <code>uuid:</code>
note that it doesn&#8217;t have to be an <span class="caps">UUID</span>.  There are several alternatives
here: for example it could be computed at built-time if the software will
only be used on a single machine, or it could be calculated using the
device&#8217;s serial number or <span class="caps">MAC</span>&nbsp;address.</p>
<p>Finally we have the <code>&lt;serviceList&gt;</code> which describes the
services this device provides.  Each service has a service type (again
there are types defined for standardised services or you can create your
own), service identifier, and three URLs.  As a service type we&#8217;re using
the standard <code>SwitchPower1</code> service.  The
<code>&lt;SCPDURL&gt;</code> field specifies where the <em>Service
Control Protocol Document</em> can be found, this describes the
service in more detail and will be covered next.  Finally there are the
control and event URLs, which need to be unique on the device and will be
managed by&nbsp;GUPnP.</p>
<h2 id="defining-services">Defining Services<a class="md-anchor" href="#defining-services" title="Permanent link"></a></h2>
<p>Because we are using a standard service we can use the service description
from the specification.  This is the <code>SwitchPower1</code>
service description&nbsp;file:</p>
<div class="codehilite"><pre><span></span><code><span class="cp">&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</span>
<span class="nt">&lt;scpd</span> <span class="na">xmlns=</span><span class="s">&quot;urn:schemas-upnp-org:service-1-0&quot;</span><span class="nt">&gt;</span>
  <span class="nt">&lt;specVersion&gt;</span>
    <span class="nt">&lt;major&gt;</span>1<span class="nt">&lt;/major&gt;</span>
    <span class="nt">&lt;minor&gt;</span>0<span class="nt">&lt;/minor&gt;</span>
  <span class="nt">&lt;/specVersion&gt;</span>
  <span class="nt">&lt;actionList&gt;</span>
    <span class="nt">&lt;action&gt;</span>
      <span class="nt">&lt;name&gt;</span>SetTarget<span class="nt">&lt;/name&gt;</span>
      <span class="nt">&lt;argumentList&gt;</span>
        <span class="nt">&lt;argument&gt;</span>
          <span class="nt">&lt;name&gt;</span>newTargetValue<span class="nt">&lt;/name&gt;</span>
          <span class="nt">&lt;relatedStateVariable&gt;</span>Target<span class="nt">&lt;/relatedStateVariable&gt;</span>
          <span class="nt">&lt;direction&gt;</span>in<span class="nt">&lt;/direction&gt;</span>
        <span class="nt">&lt;/argument&gt;</span>
      <span class="nt">&lt;/argumentList&gt;</span>
    <span class="nt">&lt;/action&gt;</span>
    <span class="nt">&lt;action&gt;</span>
      <span class="nt">&lt;name&gt;</span>GetTarget<span class="nt">&lt;/name&gt;</span>
      <span class="nt">&lt;argumentList&gt;</span>
        <span class="nt">&lt;argument&gt;</span>
          <span class="nt">&lt;name&gt;</span>RetTargetValue<span class="nt">&lt;/name&gt;</span>
          <span class="nt">&lt;relatedStateVariable&gt;</span>Target<span class="nt">&lt;/relatedStateVariable&gt;</span>
          <span class="nt">&lt;direction&gt;</span>out<span class="nt">&lt;/direction&gt;</span>
        <span class="nt">&lt;/argument&gt;</span>
      <span class="nt">&lt;/argumentList&gt;</span>
    <span class="nt">&lt;/action&gt;</span>
    <span class="nt">&lt;action&gt;</span>
      <span class="nt">&lt;name&gt;</span>GetStatus<span class="nt">&lt;/name&gt;</span>
      <span class="nt">&lt;argumentList&gt;</span>
        <span class="nt">&lt;argument&gt;</span>
          <span class="nt">&lt;name&gt;</span>ResultStatus<span class="nt">&lt;/name&gt;</span>
          <span class="nt">&lt;relatedStateVariable&gt;</span>Status<span class="nt">&lt;/relatedStateVariable&gt;</span>
          <span class="nt">&lt;direction&gt;</span>out<span class="nt">&lt;/direction&gt;</span>
        <span class="nt">&lt;/argument&gt;</span>
      <span class="nt">&lt;/argumentList&gt;</span>
    <span class="nt">&lt;/action&gt;</span>
  <span class="nt">&lt;/actionList&gt;</span>
  <span class="nt">&lt;serviceStateTable&gt;</span>
    <span class="nt">&lt;stateVariable</span> <span class="na">sendEvents=</span><span class="s">&quot;no&quot;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;name&gt;</span>Target<span class="nt">&lt;/name&gt;</span>
      <span class="nt">&lt;dataType&gt;</span>boolean<span class="nt">&lt;/dataType&gt;</span>
      <span class="nt">&lt;defaultValue&gt;</span>0<span class="nt">&lt;/defaultValue&gt;</span>
    <span class="nt">&lt;/stateVariable&gt;</span>
    <span class="nt">&lt;stateVariable</span> <span class="na">sendEvents=</span><span class="s">&quot;yes&quot;</span><span class="nt">&gt;</span>
      <span class="nt">&lt;name&gt;</span>Status<span class="nt">&lt;/name&gt;</span>
      <span class="nt">&lt;dataType&gt;</span>boolean<span class="nt">&lt;/dataType&gt;</span>
      <span class="nt">&lt;defaultValue&gt;</span>0<span class="nt">&lt;/defaultValue&gt;</span>
    <span class="nt">&lt;/stateVariable&gt;</span>
  <span class="nt">&lt;/serviceStateTable&gt;</span>
<span class="nt">&lt;/scpd&gt;</span>
</code></pre></div>

<p>Again, the <code>&lt;specVersion&gt;</code> tag defines the UPnP version
that is being used.  The rest of the document consists of an
<code>&lt;actionList&gt;</code> defining the actions available and a
<code>&lt;serviceStateTable&gt;</code> defining the state&nbsp;variables.</p>
<p>Every <code>&lt;action&gt;</code> has a <code>&lt;name&gt;</code> and a list
of <code>&lt;argument&gt;</code>s.  Arguments also have a name, a direction
(<code>in</code> or <code>out</code> for input or output
 arguments) and a related state variable.  The state variable is used to
determine the type of the argument, and as such is a required element.
This can lead to the creation of otherwise unused state variables to
define the type for an argument (the <code>WANIPConnection</code>
service is a good example of this), thanks to the legacy behind&nbsp;UPnP.</p>
<p><code>&lt;stateVariable&gt;</code>s need to specify their
<code>&lt;name&gt;</code> and <code>&lt;dataType&gt;</code>.  State variables
by default send notifications when they change, to specify that a variable
doesn&#8217;t do this set the <code>&lt;sendEvents&gt;</code> attribute to
<code>no</code>.  Finally there are optional
<code>&lt;defaultValue&gt;</code>, <code>&lt;allowedValueList&gt;</code> and
<code>&lt;allowedValueRange&gt;</code> elements which specify what the
default and valid values for the&nbsp;variable.</p>
<p>For the full specification of the service definition file, including a
complete list of valid <code>&lt;dataType&gt;</code>s, see section 2.3 of
the <a href="http://upnp.org/specs/arch/UPnP-arch-DeviceArchitecture-v1.0.pdf">UPnP Device&nbsp;Architecture</a></p>
<h2 id="implementing-the-device">Implementing the Device<a class="md-anchor" href="#implementing-the-device" title="Permanent link"></a></h2>
<p>Before starting to implement the device, some boilerplate code is needed
to initialise GUPnP. A GUPnP context can be created using <code>gupnp_context_new()</code>.</p>
<div class="codehilite"><pre><span></span><code><span class="n">GUPnPContext</span><span class="w"> </span><span class="o">*</span><span class="n">context</span><span class="p">;</span><span class="w"></span>
<span class="cm">/* Create the GUPnP context with default host and port */</span><span class="w"></span>
<span class="n">context</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">gupnp_context_new</span><span class="w"> </span><span class="p">(</span><span class="nb">NULL</span><span class="p">,</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
</code></pre></div>

<p>Next the root device can be created. The name of the device description
file can be passed as an absolute file path or a relative path to the
second parameter of <code>gupnp_root_device_new()</code>. The service description
files referenced in the device description are expected to be at the path
given there as&nbsp;well.</p>
<div class="codehilite"><pre><span></span><code><span class="n">GUPnPRootDevice</span><span class="w"> </span><span class="o">*</span><span class="n">dev</span><span class="p">;</span><span class="w"></span>
<span class="cm">/* Create the root device object */</span><span class="w"></span>
<span class="n">dev</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">gupnp_root_device_new</span><span class="w"> </span><span class="p">(</span><span class="n">context</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;BinaryLight1.xml&quot;</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;.&quot;</span><span class="p">);</span><span class="w"></span>
<span class="cm">/* Activate the root device, so that it announces itself */</span><span class="w"></span>
<span class="n">gupnp_root_device_set_available</span><span class="w"> </span><span class="p">(</span><span class="n">dev</span><span class="p">,</span><span class="w"> </span><span class="n">TRUE</span><span class="p">);</span><span class="w"></span>
</code></pre></div>

<p>GUPnP scans the device description and any service description files it
refers to, so if the main loop was entered now the device and service
would be available on the network, albeit with no functionality.  The
remaining task is to implement the&nbsp;services.</p>
<h2 id="implementing-a-service">Implementing a Service<a class="md-anchor" href="#implementing-a-service" title="Permanent link"></a></h2>
<p>To implement a service we first fetch the <code>GUPnPService</code> from the root
device using <code>gupnp_device_info_get_service()</code> (<code>GUPnPRootDevice</code> is a
subclass of <code>GUPnPDevice</code>, which implements <code>GUPnPDeviceInfo</code>).  This
returns a <code>GUPnPServiceInfo</code> which again is an interface, implemented by
<code>GUPnPService</code> (on the server) and <code>GUPnPServiceProxy</code> (on the&nbsp;client).</p>
<div class="codehilite"><pre><span></span><code><span class="n">GUPnPServiceInfo</span><span class="w"> </span><span class="o">*</span><span class="n">service</span><span class="p">;</span><span class="w"></span>
<span class="n">service</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">gupnp_device_info_get_service</span><span class="w"> </span><span class="p">(</span><span class="n">GUPNP_DEVICE_INFO</span><span class="w"> </span><span class="p">(</span><span class="n">dev</span><span class="p">),</span><span class="w"> </span><span class="s">&quot;urn:schemas-upnp-org:service:SwitchPower:1&quot;</span><span class="p">);</span><span class="w"></span>
</code></pre></div>

<p>GUPnPService handles interacting with the network itself, leaving the
implementation of the service itself to signal handlers that we need to
connect.  There are two signals: <code>GUPnPService::action-invoked</code> and
<code>GUPnPService::query-variable</code>.  <code>GUPnPService::action-invoked</code> is emitted
when a client invokes an action: the handler is passed a
<code>GUPnPServiceAction</code> object that identifies which action was invoked, and
is used to return values using <code>gupnp_service_action_set()</code>.
<code>GUPnPService::query-variable</code> is emitted for evented variables when a
control point subscribes to the service (to announce the initial value),
or whenever a client queries the value of a state variable (note that this
is now deprecated behaviour for UPnP control points): the handler is
passed the variable name and a <code>GValue</code> which should be set to the current
value of the&nbsp;variable.</p>
<p>Handlers should be targetted at specific actions or variables by using
the signal detail when connecting. For example,
this causes <code>on_get_status_action</code> to be called when
the <code>GetStatus</code> action is&nbsp;invoked:</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"> </span><span class="nf">on_get_status_action</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPService</span><span class="w"> </span><span class="o">*</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="n">GUPnPServiceAction</span><span class="w"> </span><span class="o">*</span><span class="n">action</span><span class="p">,</span><span class="w"> </span><span class="n">gpointer</span><span class="w"> </span><span class="n">user_data</span><span class="p">);</span><span class="w"></span>
<span class="c1">// ...</span>
<span class="n">g_signal_connect</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;action-invoked::GetStatus&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">G_CALLBACK</span><span class="w"> </span><span class="p">(</span><span class="n">on_get_status_action</span><span class="p">),</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
</code></pre></div>

<p>The implementation of action handlers is quite simple.  The handler is
passed a <code>GUPnPServiceAction</code> object which represents the in-progress
action.  If required it can be queried using
<code>gupnp_service_action_get_name()</code> to identify the action (this isn&#8217;t
required if detailed signals were connected).  Any
in arguments can be retrieving using
<code>gupnp_service_action_get()</code>, and then return values can be set using
<code>gupnp_service_action_set()</code>.  Once the action has been performed, either
<code>gupnp_service_action_return()</code> or <code>gupnp_service_action_return_error()</code>
should be called to either return successfully or return an error&nbsp;code.</p>
<p>If any evented state variables were modified during the action then a
notification should be emitted using <code>gupnp_service_notify()</code>.  This is an
example implementation of <code>GetStatus</code> and <code>SetTarget</code></p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span><span class="w"> </span><span class="n">gboolean</span><span class="w"> </span><span class="n">status</span><span class="p">;</span><span class="w"></span>

<span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">get_status_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPService</span><span class="w"> </span><span class="o">*</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="n">GUPnPServiceAction</span><span class="w"> </span><span class="o">*</span><span class="n">action</span><span class="p">,</span><span class="w"> </span><span class="n">gpointer</span><span class="w"> </span><span class="n">user_data</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">gupnp_service_action_set</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">,</span><span class="w"></span>
<span class="w">                              </span><span class="s">&quot;ResultStatus&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">G_TYPE_BOOLEAN</span><span class="p">,</span><span class="w"> </span><span class="n">status</span><span class="p">,</span><span class="w"></span>
<span class="w">                              </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
<span class="w">    </span><span class="n">gupnp_service_action_return</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">);</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="kt">void</span><span class="w"></span>
<span class="nf">set_target_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPService</span><span class="w"> </span><span class="o">*</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="n">GUPnPServiceAction</span><span class="w"> </span><span class="o">*</span><span class="n">action</span><span class="p">,</span><span class="w"> </span><span class="n">gpointer</span><span class="w"> </span><span class="n">user_data</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">gupnp_service_action_get</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">,</span><span class="w"></span>
<span class="w">                              </span><span class="s">&quot;NewTargetValue&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">G_TYPE_BOOLEAN</span><span class="p">,</span><span class="w"> </span><span class="o">&amp;</span><span class="n">status</span><span class="p">,</span><span class="w"></span>
<span class="w">                              </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
<span class="w">    </span><span class="n">gupnp_service_action_return</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">);</span><span class="w"></span>
<span class="w">    </span><span class="n">gupnp_service_notify</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;Status&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">G_TYPE_STRING</span><span class="p">,</span><span class="w"> </span><span class="n">status</span><span class="p">,</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="c1">//...</span>

<span class="n">g_signal_connect</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;action-invoked::GetStatus&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">G_CALLBACK</span><span class="w"> </span><span class="p">(</span><span class="n">get_status_cb</span><span class="p">),</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
<span class="n">g_signal_connect</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;action-invoked::SetTarget&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">G_CALLBACK</span><span class="w"> </span><span class="p">(</span><span class="n">set_target_cb</span><span class="p">),</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
</code></pre></div>

<p>State variable query handlers are called with the name of the variable and
a <code>GValue</code>.  This value should be initialized with the relevant type and
then set to the current value.  Again signal detail can be used to connect
handlers to specific state variable&nbsp;callbacks.</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span><span class="w"> </span><span class="n">gboolean</span><span class="w"> </span><span class="n">status</span><span class="p">;</span><span class="w"></span>

<span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">query_status_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPService</span><span class="w"> </span><span class="o">*</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="kt">char</span><span class="w"> </span><span class="o">*</span><span class="n">variable</span><span class="p">,</span><span class="w"> </span><span class="n">GValue</span><span class="w"> </span><span class="o">*</span><span class="n">value</span><span class="p">,</span><span class="w"> </span><span class="n">gpointer</span><span class="w"> </span><span class="n">user_data</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">g_value_init</span><span class="w"> </span><span class="p">(</span><span class="n">value</span><span class="p">,</span><span class="w"> </span><span class="n">G_TYPE_BOOLEAN</span><span class="p">);</span><span class="w"></span>
<span class="w">    </span><span class="n">g_value_set_boolean</span><span class="w"> </span><span class="p">(</span><span class="n">value</span><span class="p">,</span><span class="w"> </span><span class="n">status</span><span class="p">);</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="c1">// ...</span>

<span class="n">g_signal_connect</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;query-variable::Status&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">G_CALLBACK</span><span class="w"> </span><span class="p">(</span><span class="n">query_status_cb</span><span class="p">),</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
</code></pre></div>

<p>The service is now fully implemented.  To complete it, enter a GLib main
loop and wait for a client to connect.  The complete source code for this
example is available as <a href="https://gitlab.gnome.org/GNOME/gupnp/-/blob/master/examples/light-server.c">examples/light-server.c</a> in
the GUPnP&nbsp;sources.</p>
<p>For services which have many actions and variables there is a convenience
method <a href="method.Service.signals_autoconnect.html"><code>gupnp_service_signals_autoconnect()</code></a> which will automatically
connect specially named handlers to signals.  See the documentation for
full details on how it&nbsp;works.</p>
<h2 id="generating-service-specific-wrappers">Generating Service-specific Wrappers<a class="md-anchor" href="#generating-service-specific-wrappers" title="Permanent link"></a></h2>
<p>Using service-specific wrappers can simplify the implementation of a service.
Wrappers can be generated with gupnp-binding-tool
using the option <code>--mode server</code>. </p>
<p>In the following examples the wrapper has been created with
  <code>--mode server --prefix switch</code>. Please note that the callback handlers
  (<code>get_status_cb</code> and <code>set_target_cb</code>) are not automatically
  generated by gupnp-binding-tool for&nbsp;you.</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span><span class="w"> </span><span class="n">gboolean</span><span class="w"> </span><span class="n">status</span><span class="p">;</span><span class="w"></span>

<span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">get_status_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPService</span><span class="w"> </span><span class="o">*</span><span class="n">service</span><span class="p">,</span><span class="w"></span>
<span class="w">           </span><span class="n">GUPnPServiceAction</span><span class="w"> </span><span class="o">*</span><span class="n">action</span><span class="p">,</span><span class="w"></span>
<span class="w">           </span><span class="n">gpointer</span><span class="w"> </span><span class="n">user_data</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">switch_get_status_action_set</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">,</span><span class="w"> </span><span class="n">status</span><span class="p">);</span><span class="w"></span>

<span class="w">    </span><span class="n">gupnp_service_action_return</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">);</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">set_target_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPService</span><span class="w"> </span><span class="o">*</span><span class="n">service</span><span class="p">,</span><span class="w"></span>
<span class="w">           </span><span class="n">GUPnPServiceAction</span><span class="w"> </span><span class="o">*</span><span class="n">action</span><span class="p">,</span><span class="w"></span>
<span class="w">           </span><span class="n">gpointer</span><span class="w"> </span><span class="n">user_data</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">switch_set_target_action_get</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">,</span><span class="w"> </span><span class="o">&amp;</span><span class="n">status</span><span class="p">);</span><span class="w"></span>
<span class="w">    </span><span class="n">switch_status_variable_notify</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="n">status</span><span class="p">);</span><span class="w"></span>

<span class="w">    </span><span class="n">gupnp_service_action_return</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">);</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="c1">// ...</span>

<span class="n">switch_get_status_action_connect</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="n">G_CALLBACK</span><span class="p">(</span><span class="n">get_status_cb</span><span class="p">),</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
<span class="n">switch_set_target_action_connect</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="n">G_CALLBACK</span><span class="p">(</span><span class="n">set_target_cb</span><span class="p">),</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
</code></pre></div>

<p>Note how many possible problem situations that were run-time errors are 
actually compile-time errors when wrappers are used: Action names, 
argument names and argument types are easier to get correct (and available
in editor&nbsp;autocompletion).</p>
<p>State variable query handlers are implemented in a similar manner, but 
they are even simpler as the return value of the handler is the state 
variable&nbsp;value.</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span><span class="w"> </span><span class="n">gboolean</span><span class="w"></span>
<span class="nf">query_status_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPService</span><span class="w"> </span><span class="o">*</span><span class="n">service</span><span class="p">,</span><span class="w"> </span>
<span class="w">             </span><span class="n">gpointer</span><span class="w"> </span><span class="n">user_data</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="k">return</span><span class="w"> </span><span class="n">status</span><span class="p">;</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="c1">// ...</span>


<span class="n">switch_status_query_connect</span><span class="w"> </span><span class="p">(</span><span class="n">service</span><span class="p">,</span><span class="w"> </span><span class="n">query_status_cb</span><span class="p">,</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
</code></pre></div>
    </div>
  </section>
</section>


    
<div id="toc" class="toc">
  <nav aria-labelledby="toc-title">
    <p id="toc-title">Content</p>
    <ul class="toc-list">
      
        
        <li class="toc-list-item"><a href="#defining-the-device"><span class="link-text">Defining the Device</span></a></li>
        
        <li class="toc-list-item"><a href="#defining-services"><span class="link-text">Defining Services</span></a></li>
        
        <li class="toc-list-item"><a href="#implementing-the-device"><span class="link-text">Implementing the Device</span></a></li>
        
        <li class="toc-list-item"><a href="#implementing-a-service"><span class="link-text">Implementing a Service</span></a></li>
        
        <li class="toc-list-item"><a href="#generating-service-specific-wrappers"><span class="link-text">Generating Service-specific Wrappers</span></a></li>
        
      
    </ul>
  </nav>
</div>


    <section id="search" class="content hidden"></section>

    <footer>
    
    </footer>
  </div>
</body>
</html>