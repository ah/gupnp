<!--
SPDX-FileCopyrightText: The GUPnP developers

SPDX-License-Identifier: LGPL-2.1-or-later
-->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>GUPnP &ndash; 1.6: UPnP Glossary</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta charset="utf-8" />

  
  <meta property="og:type" content="website"/>

  
  <meta property="og:image:width" content="256"/>
  <meta property="og:image:height" content="256"/>
  <meta property="og:image:secure_url" content="gupnp-logo-short.svg"/>
  <meta property="og:image:alt" content="GUPnP-1.6"/>
  

  
  <meta property="og:title" content="GUPnP: UPnP Glossary"/>
  <meta property="og:description" content="Reference for GUPnP-1.6: UPnP Glossary"/>
  <meta name="twitter:title" content="GUPnP: UPnP Glossary"/>
  <meta name="twitter:description" content="Reference for GUPnP-1.6: UPnP Glossary"/>


  
  <meta name="twitter:card" content="summary"/>

  
  
  
  

  <link rel="stylesheet" href="style.css" type="text/css" />

  

  
  <script src="urlmap.js"></script>
  
  
  <script src="fzy.js"></script>
  <script src="search.js"></script>
  
  <script src="main.js"></script>
  <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
  <div id="body-wrapper" tabindex="-1">

    <nav class="sidebar devhelp-hidden">
      
      <div class="section">
        <img src="gupnp-logo-short.svg" class="logo"/>
      </div>
      
      
      <div class="search section">
        <form id="search-form" autocomplete="off">
          <input id="search-input" type="text" name="do-not-autocomplete" placeholder="Click, or press 's' to search" autocomplete="off"/>
        </form>
      </div>
      
      <div class="section namespace">
        <h3><a href="index.html">GUPnP</a></h3>
        <p>API Version: 1.6</p>
        
        <p>Library Version: 1.6.0</p>
        
      </div>
      
      
    </nav>

    <button id="btn-to-top" class="hidden"><span class="up-arrow"></span></button>

    
<section id="main" class="content">
  <section>
    <div class="docblock">
    <h1 id="action">Action<a class="md-anchor" href="#action" title="Permanent link"></a></h1>
<blockquote>
<p>An Action is a method call on a Service, which encapsulated a single piece of
functionality.  Actions can have multiple input and output arguments, and
can return error codes.  UPnP allows one of the output arguments to be
marked as the return value, but GUPnP doesn&#8217;t treat return values&nbsp;specially.</p>
<p>Every action argument has a related State Variable,
which determines the type of the argument.  Note that even if the argument
wouldn&#8217;t need a state variable it is still required, due to historical&nbsp;reasons.</p>
</blockquote>
<h1 id="control-point">Control Point<a class="md-anchor" href="#control-point" title="Permanent link"></a></h1>
<blockquote>
<p>A Control Point is an entity on the network which
communicates with other Devices and
Services.  In the client/server model the control
point is a client and the Service is a server,
although it is common for devices to also be a control point because
whilst a single control point/service connection is client/server, the
UPnP network as whole is&nbsp;peer-to-peer.</p>
</blockquote>
<h1 id="device">Device<a class="md-anchor" href="#device" title="Permanent link"></a></h1>
<blockquote>
<p>A Device is an entity on the network which
communicates using the UPnP standards.  This can be a dedicated physical
device such as a router or printer, or a <span class="caps">PC</span> which is running software
implementing the UPnP&nbsp;standards.</p>
<p>A Device can contain sub-devices, for example a combination
printer/scanner could appear as a general device with a printer
sub-device and a scanner&nbsp;sub-device.</p>
<p>Every device has zero or more Services. UPnP defines many standard
device types, which specify services which are required to be implemented.
Alternatively, a non-standard device type could be used.  Examples of
standard device types are <code>MediaRenderer</code> or
<code>InternetGatewayDevice</code>.</p>
</blockquote>
<h1 id="didl-lite">DIDL-Lite<a class="md-anchor" href="#didl-lite" title="Permanent link"></a></h1>
<blockquote>
<p>Digital Item Declaration Language -&nbsp;Lite</p>
<p>An <span class="caps">XML</span> schema used to represent digital content metadata. Defined by
the UPnP&nbsp;Forum.</p>
</blockquote>
<h1 id="service">Service<a class="md-anchor" href="#service" title="Permanent link"></a></h1>
<blockquote>
<p>A Service is a collection of related methods
(called Actions) and public variables (called
State Variables) which together form a logical interface.
     UPnP defines standard services that define actions and variables which
must be present and their semantics.  Examples of these are
<code>AVTransport</code> and <code>WANIPConnection</code>.</p>
</blockquote>
<p>See&nbsp;also:</p>
<ul>
<li><a href="#action">Action</a></li>
<li><a href="#device">Device</a></li>
<li><a href="#state-variable">State&nbsp;Variable</a></li>
</ul>
<h1 id="scdp">SCDP<a class="md-anchor" href="#scdp" title="Permanent link"></a></h1>
<blockquote>
<p>Service Control Protocol&nbsp;Document</p>
<p>An <span class="caps">XML</span> document which defines the set of <glossterm>Actions</glossterm>
and <glossterm>State Variables</glossterm> that a
<glossterm>Service</glossterm> implements.</p>
</blockquote>
<p>See&nbsp;also:</p>
<ul>
<li><a href="#action">Action</a></li>
<li><a href="#device">Device</a></li>
<li><a href="#state-variable">State&nbsp;Variable</a></li>
</ul>
<h1 id="ssdp">SSDP<a class="md-anchor" href="#ssdp" title="Permanent link"></a></h1>
<blockquote>
<p><glossterm>Simple Service Discovery Protocol</glossterm></p>
<p>UPnP device discovery protocol. Specifies how <glossterm>Devices</glossterm> 
advertise their <glossterm>Services</glossterm> in the network and also how 
<glossterm>Control Points</glossterm> search for
services and devices&nbsp;respond.</p>
</blockquote>
<p>See&nbsp;also:</p>
<ul>
<li><a href="#device">Device</a></li>
<li><a href="#controlpoint">Action</a></li>
<li><a href="#service">Service</a></li>
</ul>
<h1 id="state-variable">State Variable<a class="md-anchor" href="#state-variable" title="Permanent link"></a></h1>
<blockquote>
<p>A <firstterm>State Variable</firstterm> is a public variable exposing some
aspect of the service&#8217;s state.  State variables are typed and optionally
are <firstterm>evented</firstterm>, which means that any changes will be
notified.  Control points are said to <firstterm>subscribe</firstterm> to
a state variable to receive change&nbsp;notifications.</p>
</blockquote>
<h1 id="udn">UDN<a class="md-anchor" href="#udn" title="Permanent link"></a></h1>
<blockquote>
<p>Unique Device&nbsp;Name</p>
<p>An unique identifier which is <emphasis>unique</emphasis> for every
device but <emphasis>never changes</emphasis> for each particular&nbsp;device.</p>
<p>A common practise is to generate a unique <span class="caps">UDN</span> on first boot from a
random seed, or use some unique and persistent property such as the
device&#8217;s <span class="caps">MAC</span> address to create the <span class="caps">UDN</span>.</p>
</blockquote>
<p>See&nbsp;also:</p>
<ul>
<li><a href="#device">Device</a></li>
</ul>
    </div>
  </section>
</section>


    
<div id="toc" class="toc">
  <nav aria-labelledby="toc-title">
    <p id="toc-title">Content</p>
    <ul class="toc-list">
      
        
        <li class="toc-list-item"><a href="#action"><span class="link-text">Action</span></a></li>
          
        
        <li class="toc-list-item"><a href="#control-point"><span class="link-text">Control Point</span></a></li>
          
        
        <li class="toc-list-item"><a href="#device"><span class="link-text">Device</span></a></li>
          
        
        <li class="toc-list-item"><a href="#didl-lite"><span class="link-text">DIDL-Lite</span></a></li>
          
        
        <li class="toc-list-item"><a href="#service"><span class="link-text">Service</span></a></li>
          
        
        <li class="toc-list-item"><a href="#scdp"><span class="link-text">SCDP</span></a></li>
          
        
        <li class="toc-list-item"><a href="#ssdp"><span class="link-text">SSDP</span></a></li>
          
        
        <li class="toc-list-item"><a href="#state-variable"><span class="link-text">State Variable</span></a></li>
          
        
        <li class="toc-list-item"><a href="#udn"><span class="link-text">UDN</span></a></li>
          
        
      
    </ul>
  </nav>
</div>


    <section id="search" class="content hidden"></section>

    <footer>
    
    </footer>
  </div>
</body>
</html>