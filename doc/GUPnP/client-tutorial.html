<!--
SPDX-FileCopyrightText: The GUPnP developers

SPDX-License-Identifier: LGPL-2.1-or-later
-->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>GUPnP &ndash; 1.6: Interacting with remote UPnP devices</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta charset="utf-8" />

  
  <meta property="og:type" content="website"/>

  
  <meta property="og:image:width" content="256"/>
  <meta property="og:image:height" content="256"/>
  <meta property="og:image:secure_url" content="gupnp-logo-short.svg"/>
  <meta property="og:image:alt" content="GUPnP-1.6"/>
  

  
  <meta property="og:title" content="GUPnP: Interacting with remote UPnP devices"/>
  <meta property="og:description" content="Reference for GUPnP-1.6: Interacting with remote UPnP devices"/>
  <meta name="twitter:title" content="GUPnP: Interacting with remote UPnP devices"/>
  <meta name="twitter:description" content="Reference for GUPnP-1.6: Interacting with remote UPnP devices"/>


  
  <meta name="twitter:card" content="summary"/>

  
  
  
  

  <link rel="stylesheet" href="style.css" type="text/css" />

  

  
  <script src="urlmap.js"></script>
  
  
  <script src="fzy.js"></script>
  <script src="search.js"></script>
  
  <script src="main.js"></script>
  <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
  <div id="body-wrapper" tabindex="-1">

    <nav class="sidebar devhelp-hidden">
      
      <div class="section">
        <img src="gupnp-logo-short.svg" class="logo"/>
      </div>
      
      
      <div class="search section">
        <form id="search-form" autocomplete="off">
          <input id="search-input" type="text" name="do-not-autocomplete" placeholder="Click, or press 's' to search" autocomplete="off"/>
        </form>
      </div>
      
      <div class="section namespace">
        <h3><a href="index.html">GUPnP</a></h3>
        <p>API Version: 1.6</p>
        
        <p>Library Version: 1.6.0</p>
        
      </div>
      
      
    </nav>

    <button id="btn-to-top" class="hidden"><span class="up-arrow"></span></button>

    
<section id="main" class="content">
  <section>
    <div class="docblock">
    <h1 id="upnp-client-tutorial">UPnP Client Tutorial<a class="md-anchor" href="#upnp-client-tutorial" title="Permanent link"></a></h1>
<p>This chapter explains how to write an application which fetches the external <span class="caps">IP</span> address
from an UPnP-compliant modem. To do this, a Control Point is created, which searches for
services of the type <code>urn:schemas-upnp-org:service:WANIPConnection:1</code> which is part of
the Internet Gateway Devce&nbsp;specification.</p>
<p>As services are discovered, ServiceProxy objects are created by GUPnP to allow interaction
with the service, on which we can invoke the action <code>GetExternalIPAddress</code> to fetch the
external <span class="caps">IP</span>&nbsp;address.</p>
<h2 id="finding-services">Finding Services<a class="md-anchor" href="#finding-services" title="Permanent link"></a></h2>
<p>First, we initialize GUPnP and create a control point targeting the service type.
Then we connect a signal handler so that we are notified when services we are interested in
are&nbsp;found.</p>
<div class="codehilite"><pre><span></span><code><span class="cp">#include</span><span class="w"> </span><span class="cpf">&lt;ibgupnp/gupnp-control-point.h&gt;</span><span class="cp"></span>

<span class="k">static</span><span class="w"> </span><span class="n">GMainLoop</span><span class="w"> </span><span class="o">*</span><span class="n">main_loop</span><span class="p">;</span><span class="w"></span>

<span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">service_proxy_available_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPControlPoint</span><span class="w"> </span><span class="o">*</span><span class="n">cp</span><span class="p">,</span><span class="w"></span>
<span class="w">                            </span><span class="n">GUPnPServiceProxy</span><span class="w"> </span><span class="o">*</span><span class="n">proxy</span><span class="p">,</span><span class="w"></span>
<span class="w">                            </span><span class="n">gpointer</span><span class="w">           </span><span class="n">userdata</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">  </span><span class="cm">/* ... */</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="kt">int</span><span class="w"></span>
<span class="nf">main</span><span class="w"> </span><span class="p">(</span><span class="kt">int</span><span class="w"> </span><span class="n">argc</span><span class="p">,</span><span class="w"> </span><span class="kt">char</span><span class="w"> </span><span class="o">**</span><span class="n">argv</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">  </span><span class="n">GUPnPContext</span><span class="w"> </span><span class="o">*</span><span class="n">context</span><span class="p">;</span><span class="w"></span>
<span class="w">  </span><span class="n">GUPnPControlPoint</span><span class="w"> </span><span class="o">*</span><span class="n">cp</span><span class="p">;</span><span class="w"></span>

<span class="w">  </span><span class="cm">/* Create a new GUPnP Context.  By here we are using the default GLib main</span>
<span class="cm">     context, and connecting to the current machine&amp;apos;s default IP on an</span>
<span class="cm">     automatically generated port. */</span><span class="w"></span>
<span class="w">  </span><span class="n">context</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">gupnp_context_new</span><span class="w"> </span><span class="p">(</span><span class="nb">NULL</span><span class="p">,</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w"> </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="cm">/* Create a Control Point targeting WAN IP Connection services */</span><span class="w"></span>
<span class="w">  </span><span class="n">cp</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">gupnp_control_point_new</span><span class="w"></span>
<span class="w">    </span><span class="p">(</span><span class="n">context</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;urn:schemas-upnp-org:service:WANIPConnection:1&quot;</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="cm">/* The service-proxy-available signal is emitted when any services which match</span>
<span class="cm">     our target are found, so connect to it */</span><span class="w"></span>
<span class="w">  </span><span class="n">g_signal_connect</span><span class="w"> </span><span class="p">(</span><span class="n">cp</span><span class="p">,</span><span class="w"></span>
<span class="w">      </span><span class="s">&quot;service-proxy-available2,</span>
<span class="w">      </span><span class="n">G_CALLBACK</span><span class="w"> </span><span class="p">(</span><span class="n">service_proxy_available_cb</span><span class="p">),</span><span class="w"></span>
<span class="w">      </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="cm">/* Tell the Control Point to start searching */</span><span class="w"></span>
<span class="w">  </span><span class="n">gssdp_resource_browser_set_active</span><span class="w"> </span><span class="p">(</span><span class="n">GSSDP_RESOURCE_BROWSER</span><span class="w"> </span><span class="p">(</span><span class="n">cp</span><span class="p">),</span><span class="w"> </span><span class="n">TRUE</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="cm">/* Enter the main loop. This will start the search and result in callbacks to</span>
<span class="cm">     service_proxy_available_cb. */</span><span class="w"></span>
<span class="w">  </span><span class="n">main_loop</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">g_main_loop_new</span><span class="w"> </span><span class="p">(</span><span class="nb">NULL</span><span class="p">,</span><span class="w"> </span><span class="n">FALSE</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="n">g_main_loop_run</span><span class="w"> </span><span class="p">(</span><span class="n">main_loop</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="cm">/* Clean up */</span><span class="w"></span>
<span class="w">  </span><span class="n">g_main_loop_unref</span><span class="w"> </span><span class="p">(</span><span class="n">main_loop</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="n">g_object_unref</span><span class="w"> </span><span class="p">(</span><span class="n">cp</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="n">g_object_unref</span><span class="w"> </span><span class="p">(</span><span class="n">context</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="k">return</span><span class="w"> </span><span class="mi">0</span><span class="p">;</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>
</code></pre></div>

<h2 id="invoking-actions">Invoking Actions<a class="md-anchor" href="#invoking-actions" title="Permanent link"></a></h2>
<p>Now we have an application which searches for the service we specified and
calls <code>service_proxy_available_cb</code> for each one it
found.  To get the external <span class="caps">IP</span> address we need to invoke the
<code>GetExternalIPAddress</code> action.  This action takes no in
arguments, and has a single out argument called &#8220;NewExternalIPAddress&#8221;.
GUPnP has a set of methods to invoke actions where you pass a
<code>NULL</code>-terminated varargs list of (name, GType, value)
tuples for the in arguments, then a <code>NULL</code>-terminated
varargs list of (name, GType, return location) tuples for the out&nbsp;arguments.</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">service_proxy_available_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPControlPoint</span><span class="w"> </span><span class="o">*</span><span class="n">cp</span><span class="p">,</span><span class="w"></span>
<span class="w">                            </span><span class="n">GUPnPServiceProxy</span><span class="w"> </span><span class="o">*</span><span class="n">proxy</span><span class="p">,</span><span class="w"></span>
<span class="w">                            </span><span class="n">gpointer</span><span class="w">           </span><span class="n">userdata</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">  </span><span class="n">GError</span><span class="w"> </span><span class="o">*</span><span class="n">error</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="nb">NULL</span><span class="p">;</span><span class="w"></span>
<span class="w">  </span><span class="kt">char</span><span class="w"> </span><span class="o">*</span><span class="n">ip</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="nb">NULL</span><span class="p">;</span><span class="w"></span>
<span class="w">  </span><span class="n">GUPnPServiceProxyAction</span><span class="w"> </span><span class="o">*</span><span class="n">action</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="nb">NULL</span><span class="p">;</span><span class="w"></span>

<span class="w">  </span><span class="n">action</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">gupnp_service_proxy_action_new</span><span class="w"> </span><span class="p">(</span><span class="w"></span>
<span class="w">       </span><span class="cm">/* Action name */</span><span class="w"></span>
<span class="w">       </span><span class="s">&quot;GetExternalIPAddress&quot;</span><span class="p">,</span><span class="w"></span>
<span class="w">       </span><span class="cm">/* IN args */</span><span class="w"></span>
<span class="w">       </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="n">gupnp_service_proxy_call_action</span><span class="w"> </span><span class="p">(</span><span class="n">proxy</span><span class="p">,</span><span class="w"></span>
<span class="w">                                   </span><span class="n">action</span><span class="p">,</span><span class="w"></span>
<span class="w">                                   </span><span class="nb">NULL</span><span class="p">,</span><span class="w"></span>
<span class="w">                                   </span><span class="o">&amp;</span><span class="n">error</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="k">if</span><span class="w"> </span><span class="p">(</span><span class="n">error</span><span class="w"> </span><span class="o">!=</span><span class="w"> </span><span class="nb">NULL</span><span class="p">)</span><span class="w"> </span><span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="k">goto</span><span class="w"> </span><span class="n">out</span><span class="p">;</span><span class="w"></span>
<span class="w">  </span><span class="p">}</span><span class="w"></span>

<span class="w">  </span><span class="n">gupnp_service_proxy_action_get_result</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">,</span><span class="w"></span>
<span class="w">       </span><span class="cm">/* Error location */</span><span class="w"></span>
<span class="w">       </span><span class="o">&amp;</span><span class="n">error</span><span class="p">,</span><span class="w"></span>
<span class="w">       </span><span class="cm">/* OUT args */</span><span class="w"></span>
<span class="w">       </span><span class="s">&quot;NewExternalIPAddress&quot;</span><span class="p">,</span><span class="w"></span>
<span class="w">       </span><span class="n">G_TYPE_STRING</span><span class="p">,</span><span class="w"> </span><span class="o">&amp;</span><span class="n">ip</span><span class="p">,</span><span class="w"></span>
<span class="w">       </span><span class="nb">NULL</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="k">if</span><span class="w"> </span><span class="p">(</span><span class="n">error</span><span class="w"> </span><span class="o">==</span><span class="w"> </span><span class="nb">NULL</span><span class="p">)</span><span class="w"> </span><span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">g_print</span><span class="w"> </span><span class="p">(</span><span class="s">&quot;External IP address is %s</span><span class="se">\n</span><span class="s">&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">ip</span><span class="p">);</span><span class="w"></span>
<span class="w">    </span><span class="n">g_free</span><span class="w"> </span><span class="p">(</span><span class="n">ip</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="p">}</span><span class="w"></span>

<span class="nl">out</span><span class="p">:</span><span class="w"></span>
<span class="w">  </span><span class="k">if</span><span class="w"> </span><span class="p">(</span><span class="n">error</span><span class="w"> </span><span class="o">!=</span><span class="w"> </span><span class="nb">NULL</span><span class="p">)</span><span class="w"> </span><span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">g_printerr</span><span class="w"> </span><span class="p">(</span><span class="s">&quot;Error: %s</span><span class="se">\n</span><span class="s">&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">error</span><span class="o">-&gt;</span><span class="n">message</span><span class="p">);</span><span class="w"></span>
<span class="w">    </span><span class="n">g_error_free</span><span class="w"> </span><span class="p">(</span><span class="n">error</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="p">}</span><span class="w"></span>

<span class="w">  </span><span class="n">gupnp_service_proxy_action_unref</span><span class="w"> </span><span class="p">(</span><span class="n">action</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="n">g_main_loop_quit</span><span class="w"> </span><span class="p">(</span><span class="n">main_loop</span><span class="p">);</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>
</code></pre></div>

<p>Note that <code>gupnp_service_proxy_call_action()</code> blocks until the service has
replied.  If you need to make non-blocking calls then use
<code>gupnp_service_proxy_call_action_async()</code>, which takes a callback that will be
called from the mainloop when the reply is&nbsp;received.</p>
<h2 id="subscribing-to-state-variable-change-notifications">Subscribing to state variable change notifications<a class="md-anchor" href="#subscribing-to-state-variable-change-notifications" title="Permanent link"></a></h2>
<p>It is possible to get change notifications for the service state variables 
that have attribute <code>sendEvents="yes"</code>. We&#8217;ll demonstrate
this by modifying <code>service_proxy_available_cb</code> and using
<code>gupnp_service_proxy_add_notify()</code> to setup a notification&nbsp;callback:</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">external_ip_address_changed</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPServiceProxy</span><span class="w"> </span><span class="o">*</span><span class="n">proxy</span><span class="p">,</span><span class="w"></span>
<span class="w">                             </span><span class="k">const</span><span class="w"> </span><span class="kt">char</span><span class="w">        </span><span class="o">*</span><span class="n">variable</span><span class="p">,</span><span class="w"></span>
<span class="w">                             </span><span class="n">GValue</span><span class="w">            </span><span class="o">*</span><span class="n">value</span><span class="p">,</span><span class="w"></span>
<span class="w">                             </span><span class="n">gpointer</span><span class="w">           </span><span class="n">userdata</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">  </span><span class="n">g_print</span><span class="w"> </span><span class="p">(</span><span class="s">&quot;External IP address changed: %s</span><span class="se">\n</span><span class="s">&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">g_value_get_string</span><span class="w"> </span><span class="p">(</span><span class="n">value</span><span class="p">));</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">service_proxy_available_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPControlPoint</span><span class="w"> </span><span class="o">*</span><span class="n">cp</span><span class="p">,</span><span class="w"></span>
<span class="w">                            </span><span class="n">GUPnPServiceProxy</span><span class="w"> </span><span class="o">*</span><span class="n">proxy</span><span class="p">,</span><span class="w"></span>
<span class="w">                            </span><span class="n">gpointer</span><span class="w">           </span><span class="n">userdata</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">  </span><span class="n">g_print</span><span class="w"> </span><span class="p">(</span><span class="s">&quot;Found a WAN IP Connection service</span><span class="se">\n</span><span class="s">&quot;</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="n">gupnp_service_proxy_set_subscribed</span><span class="w"> </span><span class="p">(</span><span class="n">proxy</span><span class="p">,</span><span class="w"> </span><span class="n">TRUE</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="k">if</span><span class="w"> </span><span class="p">(</span><span class="o">!</span><span class="n">gupnp_service_proxy_add_notify</span><span class="w"> </span><span class="p">(</span><span class="n">proxy</span><span class="p">,</span><span class="w"></span>
<span class="w">                                       </span><span class="s">&quot;ExternalIPAddress&quot;</span><span class="p">,</span><span class="w"></span>
<span class="w">                                       </span><span class="n">G_TYPE_STRING</span><span class="p">,</span><span class="w"></span>
<span class="w">                                       </span><span class="n">external_ip_address_changed</span><span class="p">,</span><span class="w"></span>
<span class="w">                                       </span><span class="nb">NULL</span><span class="p">))</span><span class="w"> </span><span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">g_printerr</span><span class="w"> </span><span class="p">(</span><span class="s">&quot;Failed to add notify&quot;</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="p">}</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>
</code></pre></div>

<h2 id="generating-wrappers">Generating wrappers<a class="md-anchor" href="#generating-wrappers" title="Permanent link"></a></h2>
<p>Using <code>gupnp_service_proxy_call_action()</code> and <code>gupnp_service_proxy_add_notify()</code>
can become tedious, because of the requirement to specify the types and deal
with GValues.  An
alternative is to use <code>gupnp-binding-tool</code>, which
generates wrappers that hide the boilerplate code from you.  Using a 
wrapper generated with prefix &#8220;ipconn&#8221; would replace
<code>gupnp_service_proxy_call_action()</code> with this&nbsp;code:</p>
<div class="codehilite"><pre><span></span><code><span class="n">ipconn_get_external_ip_address</span><span class="w"> </span><span class="p">(</span><span class="n">proxy</span><span class="p">,</span><span class="w"> </span><span class="o">&amp;</span><span class="n">ip</span><span class="p">,</span><span class="w"> </span><span class="o">&amp;</span><span class="n">error</span><span class="p">);</span><span class="w"></span>
</code></pre></div>

<p>State variable change notifications are friendlier with wrappers as&nbsp;well:</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">external_ip_address_changed</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPServiceProxy</span><span class="w"> </span><span class="o">*</span><span class="n">proxy</span><span class="p">,</span><span class="w"></span>
<span class="w">                             </span><span class="k">const</span><span class="w"> </span><span class="n">gchar</span><span class="w">       </span><span class="o">*</span><span class="n">external_ip_address</span><span class="p">,</span><span class="w"></span>
<span class="w">                             </span><span class="n">gpointer</span><span class="w">           </span><span class="n">userdata</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">  </span><span class="n">g_print</span><span class="w"> </span><span class="p">(</span><span class="s">&quot;External IP address changed: &amp;apos;%s&amp;apos;</span><span class="se">\n</span><span class="s">&quot;</span><span class="p">,</span><span class="w"> </span><span class="n">external_ip_address</span><span class="p">);</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>

<span class="k">static</span><span class="w"> </span><span class="kt">void</span><span class="w"></span>
<span class="nf">service_proxy_available_cb</span><span class="w"> </span><span class="p">(</span><span class="n">GUPnPControlPoint</span><span class="w"> </span><span class="o">*</span><span class="n">cp</span><span class="p">,</span><span class="w"></span>
<span class="w">                            </span><span class="n">GUPnPServiceProxy</span><span class="w"> </span><span class="o">*</span><span class="n">proxy</span><span class="w"></span>
<span class="w">                            </span><span class="n">gpointer</span><span class="w">           </span><span class="n">userdata</span><span class="p">)</span><span class="w"></span>
<span class="p">{</span><span class="w"></span>
<span class="w">  </span><span class="n">g_print</span><span class="w"> </span><span class="p">(</span><span class="s">&quot;Found a WAN IP Connection service</span><span class="se">\n</span><span class="s">&quot;</span><span class="p">);</span><span class="w"></span>

<span class="w">  </span><span class="n">gupnp_service_proxy_set_subscribed</span><span class="w"> </span><span class="p">(</span><span class="n">proxy</span><span class="p">,</span><span class="w"> </span><span class="n">TRUE</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="k">if</span><span class="w"> </span><span class="p">(</span><span class="o">!</span><span class="n">ipconn_external_ip_address_add_notify</span><span class="w"> </span><span class="p">(</span><span class="n">proxy</span><span class="p">,</span><span class="w"></span>
<span class="w">                                              </span><span class="n">external_ip_address_changed</span><span class="p">,</span><span class="w"></span>
<span class="w">                                              </span><span class="nb">NULL</span><span class="p">))</span><span class="w"> </span><span class="p">{</span><span class="w"></span>
<span class="w">    </span><span class="n">g_printerr</span><span class="w"> </span><span class="p">(</span><span class="s">&quot;Failed to add notify&quot;</span><span class="p">);</span><span class="w"></span>
<span class="w">  </span><span class="p">}</span><span class="w"></span>
<span class="p">}</span><span class="w"></span>
</code></pre></div>
    </div>
  </section>
</section>


    
<div id="toc" class="toc">
  <nav aria-labelledby="toc-title">
    <p id="toc-title">Content</p>
    <ul class="toc-list">
      
        
        <li class="toc-list-item"><a href="#finding-services"><span class="link-text">Finding Services</span></a></li>
        
        <li class="toc-list-item"><a href="#invoking-actions"><span class="link-text">Invoking Actions</span></a></li>
        
        <li class="toc-list-item"><a href="#subscribing-to-state-variable-change-notifications"><span class="link-text">Subscribing to state variable change notifications</span></a></li>
        
        <li class="toc-list-item"><a href="#generating-wrappers"><span class="link-text">Generating wrappers</span></a></li>
        
      
    </ul>
  </nav>
</div>


    <section id="search" class="content hidden"></section>

    <footer>
    
    </footer>
  </div>
</body>
</html>